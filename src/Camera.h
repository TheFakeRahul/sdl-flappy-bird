#ifndef CAMERA_H
#define CAMERA_H

#include "Vector2.h"
#include "SDL.h"

class Camera {
public:
	Camera(float orthoSize);
	~Camera();
	void Render();

	static Camera* Instance;

	void SetOrthoSize(float orthoSize);
	float GetOrthoSize() { return orthoSize; }
	float GetUnitSize() { return unitSize; }


	Vector2 WorldToScreenUnits(Vector2 v);
	Vector2 ScreenToWorldUnits(Vector2 v);

	Vector2 position;

private:
	float orthoSize;
	float unitSize;
	Vector2 camSize;
};
#endif // !CAMERA_H