#include "Point.h"

Point::Point() {
  x = y = 0;
}

Point::Point(int a) {
  x = y = a;
}

Point::Point(int x, int y) {
  this->x = x;
  this->y = y;
}

Point Point::One() {
  return Point(1);
}

Point Point::Hundred() {
  return Point(100);
}

Point Point::Left() {
  return Point(-1, 0);
}

Point Point::Right() {
  return Point(1, 0);
}
Point Point::Up() {
  return Point(0, 1);
}

Point Point::Down() {
  return Point(0, -1);
}

Vector2 Point::ToVector2() {
  return Vector2(x, y);
}