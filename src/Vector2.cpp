#include "Vector2.h"

Vector2::Vector2() {
  x = y = 0;
}

Vector2::Vector2(float a) {
  x = y = a;
}

Vector2::Vector2(float x, float y) {
  this->x = x;
  this->y = y;
}

Vector2 Vector2::One() {
  return Vector2(1.0f);
}

Vector2 Vector2::Hundred() {
  return Vector2(100.0f);
}

Vector2 Vector2::Left() {
  return Vector2(-1.0f, 0.0f);
}

Vector2 Vector2::Right() {
  return Vector2(1.0f, 0.0f);
}
Vector2 Vector2::Up() {
  return Vector2(0.0f, 1.0f);
}

Vector2 Vector2::Down() {
  return Vector2(0.0f, -1.0f);
}