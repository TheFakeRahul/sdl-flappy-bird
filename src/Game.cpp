#include "Game.h"

int Game::Width = 0;		// The width and height of the window.
int Game::Height = 0;		// This will be used for orthographic rendering.
SDL_Renderer* Game::renderer = nullptr;

Game::Game(const char* title, int width, int height, bool fullscreen) {
	// Set pointers to null
	renderer = nullptr;
	window = nullptr;

	Game::Width = width;
	Game::Height = height;
	std::cout << Game::Width << ", " << Game::Height << '\n';

	// Initialise SDL then create window and renderer
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
		Point pos = Point(SDL_WINDOWPOS_CENTERED);
		int flags = fullscreen ? SDL_WINDOW_FULLSCREEN : 0;
		window = SDL_CreateWindow(title, pos.x, pos.y, width, height, flags);
		if (window) std::cout << "Window created.\n";

		renderer = SDL_CreateRenderer(window, -1, 0);
		if (renderer) std::cout << "Renderer created.\n";
		isRunning = true;
	}
}

Game::~Game() {
	// Free all memory used in the game
}

void Game::Initialise() {
	// Initialise GameObjects here
}

void Game::Update() {
	// Add update logic here
}

// Handle input events
void Game::HandleEvents() {
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		switch (event.key.keysym.sym) {
			case SDLK_ESCAPE: 
				isRunning = false;
				break;
		}
		if (event.type == SDL_QUIT) isRunning = false;
	}
}

void Game::Render() {
	SDL_SetRenderDrawColor(renderer, 15, 15, 15, 255);	//Set the background colour
	SDL_RenderClear(renderer);							//Clear the render view

	// TODO: Add render logic here

	SDL_RenderPresent(renderer);						//Render the frame
}

// This function cleans the game when it's closed
void Game::Clean() {
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit();
	std::cout << "Game cleaned.\n";
}