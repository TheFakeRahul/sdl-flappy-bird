#ifndef VECTOR2_H
#define VECTOR2_H

class Vector2 {
public:
  Vector2();
  Vector2(float x, float y);
  Vector2(float a);

  static Vector2 One();
  static Vector2 Hundred();

  static Vector2 Left();
  static Vector2 Right();
  static Vector2 Up();
  static Vector2 Down();

  // TODO: Add operator methods here

  // TODO: Add math functions here

  float x, y;
};

#endif // !VECTOR2_H
