#ifndef GAME_H
#define GAME_H

#include "SDL.h"
#include "SDL_image.h"

#include "Point.h"
#include <iostream>

class Game {
public:
	Game(const char* title, int width, int height, bool fullscreen);
	~Game();

	void Initialise();
	void Update();
	void HandleEvents();
	void Render();
	void Clean();
	bool Running() { return isRunning; }

	static SDL_Renderer* renderer;
	static int Width, Height;

private:
	bool isRunning = false;
	bool isMainMenu = true;
	SDL_Window* window;
};

#endif // !GAME_H