#include "Camera.h"
#include "Game.h"

Camera* Camera::Instance = nullptr;

Camera::Camera(float orthoSize)
{
	if (Instance == nullptr) Camera::Instance = this;
	else delete this;
	
	SetOrthoSize(orthoSize);
	this->position = Vector2(0.0f);
}

Camera::~Camera() {
	if (Instance == this) Instance = nullptr;
}

// This function just renders a square to visualise the size of one unit
// It should be replaced with a grid in the future
void Camera::Render() {
	SDL_Renderer* renderer = Game::renderer;
	Vector2 updatedPos = WorldToScreenUnits(position);
	Vector2 topLeft(updatedPos.x - (unitSize / 2.0f), updatedPos.y - (unitSize / 2.0f));
	Vector2 bottomRight(updatedPos.x + (unitSize / 2.0f), updatedPos.y + (unitSize / 2.0f));
	SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
	SDL_RenderDrawLine(renderer, topLeft.x, topLeft.y, bottomRight.x, topLeft.y);
	SDL_RenderDrawLine(renderer, bottomRight.x, topLeft.y, bottomRight.x, bottomRight.y);
	SDL_RenderDrawLine(renderer, bottomRight.x, bottomRight.y, topLeft.x, bottomRight.y);
	SDL_RenderDrawLine(renderer, topLeft.x, bottomRight.y, topLeft.x, topLeft.y);
}

void Camera::SetOrthoSize(float orthoSize) {
	this->orthoSize = orthoSize;
	this->unitSize = Game::Height / (orthoSize * 2.0f);

	camSize = Vector2(Game::Width / unitSize, Game::Height / unitSize);
}

// Convert from world units to screen coordinates
Vector2 Camera::WorldToScreenUnits(Vector2 v) {
	return Vector2((Game::Width / 2) + (v.x * unitSize) - (position.x * unitSize), (Game::Height / 2) - (v.y * unitSize) + (position.y * unitSize));
}

// Convert from screen coordinates to world units
Vector2 Camera::ScreenToWorldUnits(Vector2 v) {
	return Vector2(position.x + (v.x / unitSize) - (camSize.x / 2.0f), position.y - (v.y / unitSize) + (camSize.y / 2.0f));
}