#include "Time.h"

double Time::deltaTime = 0;
Uint64 Time::LAST = 0;
Uint64 Time::NOW = 0;

void Time::CalculateDeltaTime() {
	NOW = SDL_GetPerformanceCounter();
	if (LAST == 0) LAST = NOW;

	// Calculate delta time
	deltaTime = (double)((NOW - LAST) / (double)SDL_GetPerformanceFrequency());

	LAST = NOW;
}