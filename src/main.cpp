#define CATCH_CONFIG_RUNNER
#include "../tests/catch.hpp"

#include "Game.h"
#include "Time.h"

Game* game = nullptr;
bool runTests = false;

int main(int args, char* argv[]) {
	if (runTests) {
		int result = Catch::Session().run(args, argv);
		return result;
	}

	// Change this to alter the window size to best suit your monitor
	unsigned int MAX_HEIGHT = 900;

	// Calculate the width and height of the window to be 16:9 
	unsigned int HEIGHT = MAX_HEIGHT - (MAX_HEIGHT % 16);
	unsigned int WIDTH = (HEIGHT / 16) * 9;

	// Initialise the game
	game = new Game("SDL Flappy Bird", WIDTH, HEIGHT, false);	
	game->Initialise();

	// Run the game loop
	while (game->Running()) {
		game->HandleEvents();			// Check for events (input)
		game->Update();						// Update the game
		game->Render();						// Render the game

		Time::CalculateDeltaTime();
	}

	// Clean the game if it's not running
	game->Clean();

	return 0;
}