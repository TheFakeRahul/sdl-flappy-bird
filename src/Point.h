#ifndef POINT_H
#define POINT_H

#include "Vector2.h"

class Point : public Vector2 {
public:
  Point();
  Point(int x, int y);
  Point(int a);

  static Point One();
  static Point Hundred();

  static Point Left();
  static Point Right();
  static Point Up();
  static Point Down();

  Vector2 ToVector2();

  // TODO: Add operator methods here

  // TODO: Add math functions here

  int x, y;
};

#endif // !POINT_H
