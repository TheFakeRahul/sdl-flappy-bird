#ifndef TIME_H
#define TIME_H

#include <SDL.h>

class Time {
public:
	static double GetDeltaTime() { return deltaTime; }
	static void CalculateDeltaTime();

private:
	static double deltaTime;	// The difference in time between frames

	static Uint64 NOW;		// Ticks at beginning of last frame
	static Uint64 LAST;		// Ticks at end of last frame
};
#endif // !TIME_H