#include "catch.hpp"
#include "../src/Point.h"

TEST_CASE("Point constructors works as expected") {
  SECTION("No constructor") {
    Point p;
    REQUIRE(p.x == 0);
    REQUIRE(p.y == 0);
  }

  SECTION("Default constructor") {
    Point p = Point();
    REQUIRE(p.x == 0);
    REQUIRE(p.y == 0);
  }

  SECTION("Single constructor") {
    Point p = Point(5);
    REQUIRE(p.x == 5);
    REQUIRE(p.y == 5);
  }

  SECTION("Double constructor") {
    Point p = Point(5, -17);
    REQUIRE(p.x == 5);
    REQUIRE(p.y == -17);
  }
}

TEST_CASE("point variables can be reassigned") {
  Point p = Point();
  SECTION("Reassign default constructor") {
    REQUIRE(p.x == 0);
    REQUIRE(p.y == 0);

    p.x = 17;
    REQUIRE(p.x == 17);

    p.y = -1089;
    REQUIRE(p.y == -1089);
  }

  SECTION("Reassign single constructor") {
    p = Point(-19);
    REQUIRE(p.x == -19);
    REQUIRE(p.y == -19);

    p.x = -500;
    REQUIRE(p.x == -500);

    p.y = 104;
    REQUIRE(p.y == 104);
  }

  SECTION("Reassign double constructor") {
    p = Point(19, 18);
    REQUIRE(p.x == 19);
    REQUIRE(p.y == 18);

    p.x = 13;
    REQUIRE(p.x == 13);

    p.y = -64;
    REQUIRE(p.y == -64);
  }
}

TEST_CASE("Direction points assign correct palues") {
  Point p = Point();
  SECTION("Left") {
    p = Point::Left();
    REQUIRE(p.x == -1);
    REQUIRE(p.y == 0);
  }

  SECTION("Right") {
    p = Point::Right();
    REQUIRE(p.x == 1);
    REQUIRE(p.y == 0);
  }

  SECTION("Up") {
    p = Point::Up();
    REQUIRE(p.x == 0);
    REQUIRE(p.y == 1);
  }

  SECTION("Down") {
    p = Point::Down();
    REQUIRE(p.x == 0);
    REQUIRE(p.y == -1);
  }
}

TEST_CASE("Number points assign correct palues") {
  Point p = Point();
  SECTION("One") {
    p = Point::One();
    REQUIRE(p.x == 1);
    REQUIRE(p.y == 1);
  }

  SECTION("Hundred") {
    p = Point::Hundred();
    REQUIRE(p.x == 100);
    REQUIRE(p.y == 100);
  }
}

TEST_CASE("Point is correctly converted to Vector2") {
  Point p = Point(-113, 107);
  Vector2 v = p.ToVector2();
  REQUIRE(v.x == -113);
  REQUIRE(v.y == 107);
}