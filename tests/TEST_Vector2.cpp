#include "catch.hpp"
#include "../src/Vector2.h"

TEST_CASE("Vector2 constructors works as expected") {
  SECTION("No constructor") {
    Vector2 v;
    REQUIRE(v.x == 0);
    REQUIRE(v.y == 0);
  }

  SECTION("Default constructor") {
    Vector2 v = Vector2();
    REQUIRE(v.x == 0);
    REQUIRE(v.y == 0);
  }

  SECTION("Single constructor") {
    Vector2 v = Vector2(4.71f);
    REQUIRE(v.x == 4.71f);
    REQUIRE(v.y == 4.71f);
  }

  SECTION("Double constructor") {
    Vector2 v = Vector2(5, -3.97f);
    REQUIRE(v.x == 5);
    REQUIRE(v.y == -3.97f);
  }
}

TEST_CASE("Vector variables can be reassigned") {
  Vector2 v = Vector2();  
  SECTION("Reassign default constructor") {
    REQUIRE(v.x == 0);
    REQUIRE(v.y == 0);

    v.x = 5.0f;
    REQUIRE(v.x == 5.0f);

    v.y = 6.7f;
    REQUIRE(v.y == 6.7f);
  }

  SECTION("Reassign single constructor") {
    v = Vector2(5.7f);
    REQUIRE(v.x == 5.7f);
    REQUIRE(v.y == 5.7f);

    v.x = -7.4f;
    REQUIRE(v.x == -7.4f);

    v.y = 14;
    REQUIRE(v.y == 14);
  }

  SECTION("Reassign double constructor") {
    v = Vector2(17, 108.3f);
    REQUIRE(v.x == 17);
    REQUIRE(v.y == 108.3f);

    v.x = 13.0f;
    REQUIRE(v.x == 13.0f);

    v.y = -64;
    REQUIRE(v.y == -64);
  }
}

TEST_CASE("Direction vectors assign correct values") {
  Vector2 v = Vector2();
  SECTION("Left") {
    v = Vector2::Left();
    REQUIRE(v.x == -1.0f);
    REQUIRE(v.y == 0.0f);
  }

  SECTION("Right") {
    v = Vector2::Right();
    REQUIRE(v.x == 1.0f);
    REQUIRE(v.y == 0.0f);
  }

  SECTION("Up") {
    v = Vector2::Up();
    REQUIRE(v.x == 0.0f);
    REQUIRE(v.y == 1.0f);
  }

  SECTION("Down") {
    v = Vector2::Down();
    REQUIRE(v.x == 0.0f);
    REQUIRE(v.y == -1.0f);
  }
}

TEST_CASE("Number vectors assign correct values") {
  Vector2 v = Vector2();
  SECTION("One") {
    v = Vector2::One();
    REQUIRE(v.x == 1.0f);
    REQUIRE(v.y == 1.0f);
  }

  SECTION("Hundred") {
    v = Vector2::Hundred();
    REQUIRE(v.x == 100.0f);
    REQUIRE(v.y == 100.0f);
  }
}