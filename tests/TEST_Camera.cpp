#include "catch.hpp"
#include "../src/Game.h"
#include "../src/Camera.h"

TEST_CASE("Camera Initialisation") {
  SECTION("Camera Instance is nullptr by default") {
    REQUIRE(Camera::Instance == nullptr);
  }

  Camera* cam = new Camera(5.0f);
  SECTION("Camera Instance is not nullptr after being declared") {
    REQUIRE(Camera::Instance != nullptr);
  }

  delete(cam);
  SECTION("Camera Instance is nullptr after being deleted") {
    Camera::Instance == nullptr;
  }
}

TEST_CASE("Orthographic size stored and unit size correctly calculated") {
  Game::Width = 1000;
  Game::Height = 500;
  Camera* cam = new Camera(10.0f);

  SECTION("Orthographic size is saved") {
    REQUIRE(cam->GetOrthoSize() == 10.0f);
  }

  SECTION("Unit size is calculated correctly") {
    // Unit size = height / (orthoSize * 2)
    REQUIRE(cam->GetUnitSize() == 25.0f);

    cam->SetOrthoSize(100.0f);
    REQUIRE(cam->GetUnitSize() == 2.5f);

    // This is very much an edge case. Probably would not want a negative orthographic size
    cam->SetOrthoSize(-20.0f);
    REQUIRE(cam->GetUnitSize() == -12.5f);
  }
  delete(cam);
}

TEST_CASE("World to Screen units correctly converted") {
  Game::Width = 1200;
  Game::Height = 600;
  Camera* cam = new Camera(5.0f);
  // One unit = 60 pixels

  Vector2 v = cam->WorldToScreenUnits(Vector2(0));
  REQUIRE(v.x == 600.0f);
  REQUIRE(v.y == 300.0f);

  v = cam->WorldToScreenUnits(Vector2(2, 1));
  REQUIRE(v.x == 720.0f);
  REQUIRE(v.y == 240.0f);

  cam->position = Vector2(2, 1);
  v = cam->WorldToScreenUnits(Vector2(2, 1));
  REQUIRE(v.x == 600.0f);
  REQUIRE(v.y == 300.0f);

  cam->position = Vector2(-3, 5);
  v = cam->WorldToScreenUnits(Vector2(-2, 7));
  REQUIRE(v.x == 660.0f);
  REQUIRE(v.y == 180.0f);
}

TEST_CASE("Screen to World units correctly converted") {
  Game::Width = 1200;
  Game::Height = 600;
  Camera* cam = new Camera(5.0f);
  // One unit = 60 pixels

  Vector2 v = cam->ScreenToWorldUnits(Vector2(600.0f, 300.0f));
  REQUIRE(v.x == 0.0f);
  REQUIRE(v.y == 0.0f);

  v = cam->ScreenToWorldUnits(Vector2(720.0f, 240.0f));
  REQUIRE(v.x == 2.0f);
  REQUIRE(v.y == 1.0f);

  cam->position = Vector2(2.0f, 1.0f);
  v = cam->ScreenToWorldUnits(Vector2(600.0f, 300.0f));
  REQUIRE(v.x == 2.0f);
  REQUIRE(v.y == 1.0f);

  cam->position = Vector2(500.0f, 100.0f);
  v = cam->ScreenToWorldUnits(Vector2(720.0f, 360.0f));
  REQUIRE(v.x == 502.0f);
  REQUIRE(v.y == 99.0f);
}