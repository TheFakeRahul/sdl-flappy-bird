# SDL Flappy Bird

A simple Flappy Bird clone built using C++ and SDL as a learning project.

# Setup guide

When you download the project it will not work immediately as you will need to download development libraries for [SDL2](https://www.libsdl.org/download-2.0.php) and [SDL2_Image](https://www.libsdl.org/tmp/SDL_image/).

## SDL2

The recommended version of SDL2 (as used in this game) is SDL2-2.0.12, however, recent updates should work fine too. From the website, you will need to download the Visual C++ 32/64-bit development libraries and extract the zip file. The extracted folder should contain another folder called `SDL2-2.0.12`, rename this folder to `SDL2`. 

## SDL2_Image

The recommended version of SDL2_Image (as used in this game) is version 2.0.5, however, recent updates should work fine too. From the website, you will need to download the Visual C++ 32/64-bit development libraries and extract the zip file. The extracted folder should contain another folder called `SDL2_image-2.0.5`, rename this folder to `SDL2_image`. 

## After Extraction

After extracting the zip files, move the renamed folders into a new folder titled `_Libraries` placed at the root of the project. Once this is done in Visual Studio you should be able to run the game in x64, however, if you run into any issues please let me know and I can help set up a better structure.